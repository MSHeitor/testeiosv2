//
//  UserAccount.swift
//  TesteiOSv2Acc
//
//  Created by Marlon Santos Heitor on 23/05/19.
//  Copyright © 2019 Marlon Santos Heitor. All rights reserved.
//

struct UserAccount: Codable {
    let userId: Int?
    let name: String?
    let bankAccount: String?
    let agency: String?
    let balance: Double?
}
