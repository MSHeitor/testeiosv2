//
//  ServiceError.swift
//  TesteiOSv2Acc
//
//  Created by Marlon Santos Heitor on 23/05/19.
//  Copyright © 2019 Marlon Santos Heitor. All rights reserved.
//

struct ServiceError: Codable {
    let code: Int?
    let message: String?
}
