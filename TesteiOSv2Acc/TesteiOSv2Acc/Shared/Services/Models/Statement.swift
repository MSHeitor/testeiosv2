//
//  Statement.swift
//  TesteiOSv2Acc
//
//  Created by Marlon Santos Heitor on 25/05/19.
//  Copyright © 2019 Marlon Santos Heitor. All rights reserved.
//

struct Statement: Codable {
    let title: String?
    let desc: String?
    let date: String?
    let value: Double?
}
