//
//  StatementsTableViewHeaderTableViewCell.swift
//  TesteiOSv2Acc
//
//  Created by Marlon Santos Heitor on 26/05/19.
//  Copyright © 2019 Marlon Santos Heitor. All rights reserved.
//

import UIKit

class StatementsTableViewHeaderTableViewCell: UITableViewCell {

    static let identifier = "StatementsTableViewHeaderTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
