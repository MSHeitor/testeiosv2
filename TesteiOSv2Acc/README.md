Para executar o projeto, é necessário ter as seguintes ferramentas instaladas:

- Git - Já vem instalado no Mac OS
- Cocoapods - https://cocoapods.org
- Xcode - Baixar na Mac App Store

Após ter as ferramentas instaladas, seguir os seguintes passos:
<ol>
<li>Baixar o código do projeto com o comando 'git clone' ou baixar o código como '.zip'</li>
<li>Abrir terminal na raiz do projeto (TesteiOSv2Acc) e executar o comando 'pod install'</li>
<li>Abrir o arquivo 'TesteiOSv2Acc.xcworkspace' e executar o projeto</li>
</ol>
