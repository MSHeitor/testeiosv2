//
//  LoginInteractorTests.swift
//  TesteiOSv2Acc
//
//  Created by Marlon Santos Heitor on 28/05/19.
//  Copyright (c) 2019 Marlon Santos Heitor. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

@testable import TesteiOSv2Acc
import XCTest

class LoginInteractorTests: XCTestCase
{
    // MARK: - Subject under test
    
    var sut: LoginInteractor!
    
    // MARK: - Test lifecycle
    
    override func setUp()
    {
        super.setUp()
        setupLoginInteractor()
    }
    
    override func tearDown()
    {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupLoginInteractor()
    {
        sut = LoginInteractor()
    }
    
    // MARK: - Test doubles
    
    class LoginPresentationLogicSpy: LoginPresentationLogic
    {
        var expectation: XCTestExpectation?
        
        var presentLoadLoginDataResponse: Login.LoadLoginData.Response? = nil
        var presentLoadLoginDataCalled = false
        func presentLoadLoginData(response: Login.LoadLoginData.Response) {
            presentLoadLoginDataCalled = true
            presentLoadLoginDataResponse = response
        }
        
        var presentLoginUserResponse: Login.LoginUser.Response? = nil
        var presentLoginUserCalled = false
        func presentLoginUser(response: Login.LoginUser.Response) {
            presentLoginUserCalled = true
            presentLoginUserResponse = response
            expectation?.fulfill()
        }
    }
    
    // MARK: - Tests
    
    func testDoLogin()
    {
        // Given
        let spy = LoginPresentationLogicSpy()
        sut.presenter = spy
        let request = Login.LoginUser.Request(user: "a@a.com", password: "123!A")
        spy.expectation = expectation(description: "DoLogin")
        
        // When
        sut.doLogin(request: request)
        
        waitForExpectations(timeout: 30, handler: nil)
        
        // Then
        XCTAssertTrue(spy.presentLoginUserCalled)
        XCTAssertTrue(spy.presentLoginUserResponse?.userAccount?.userId != nil)
        XCTAssertTrue(spy.presentLoginUserResponse?.serviceError?.code == nil)
    }
    
    func testDoLoginInvalidUser()
    {
        // Given
        let spy = LoginPresentationLogicSpy()
        sut.presenter = spy
        let request = Login.LoginUser.Request(user: "123", password: "123!A")
        
        // When
        sut.doLogin(request: request)
        
        // Then
        XCTAssertTrue(spy.presentLoginUserCalled)
        XCTAssertTrue(spy.presentLoginUserResponse?.userAccount?.userId == nil)
        XCTAssertTrue(spy.presentLoginUserResponse?.serviceError?.code != nil)
    }
    
}
